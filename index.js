// Bài 1: Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần

/**
 Mô hìn h3 khối
 Input: tạo ra 3 nút để người dùng nhập các số nguyên

 Xử lí: 
 sẽ xét cặp các trường hợp theo từng số N 
 số N1 (n1>n2; n1>n3; n2>n3) => (n3,n2,n1) ; (n1>n2; n1>n3; n3>n2) => (n2,n3,n1)
 số N2 (n2>n1; n2>n3; n1>n3) => (n3,n1,n2) ; (n2>n1; n2>n3; n3>n1) => (n1,n3,n2)
 số N3 (n3>n1; n3>n2; n1>n2) => (n2,n1,n3) ; (n3>n1; n3>n2; n2>n1) => (n1,n2,n3)

 output: sẽ cho ra các số tăng dần và render ra HTML
 */

document.getElementById('btn-xuat-so').addEventListener('click',
    function xuatSo(){
    //khai báo biến
     var N1 = document.getElementById('txt-songuyen-1').value * 1
     var N2 = document.getElementById('txt-songuyen-2').value * 1
     var N3 = document.getElementById('txt-songuyen-3').value * 1

    var result;
    if (N1 >= N2 && N1 >= N3 && N2 >= N3) 
    {
        result = N1 + " > " + N2 + " > " + N3;
        console.log("result:",result)    
    } 
    else if (N1 >= N2 && N1 >= N3 && N3 >= N2)
    {
        result = N1 + " > " + N3 + " > " + N2;
        console.log("result:",result)  
    } 
    else if (N2 >= N1 && N2 > N3 && N1 > N3) 
    {
        result = N2 + " > " + N1 + " > " + N3;
        console.log("result:",result)    
    } 
    else if (N2 >= N1 && N2 > N3 && N3 > N1) 
    {
        result = N2 + " > " + N3 + " > " + N1;
        console.log("result:",result)    
    } 
    else if (N3 >= N1 && N3 >= N2 && N1 >= N2) 
    {
        result = N3 + " > " + N1 + " > " + N2;
        console.log("result:",result)    
    } 
    else {
        result = N3 + " > " + N2 + " > " + N1;
        console.log("result:",result)    
    }

    document.querySelector('.txt-xuat-so').innerHTML = `<div>${result}</div>`
})


// Bài 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ
// hỏi ai sử dụng máy. Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp. Giả sử trong gia
// đình có 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)

/**
 Mô hìn h3 khối
 Input: tạo ra biến để lấy value trong thẻ select

 Xử lí: 
tạo switch để lọc giá trị và gán giá trị cho cho biến

 output: sẽ cho ra các số tăng dần và render ra HTML
 */
 document.querySelector('.btn-send-hi').addEventListener('click', function guiLoiChao(){

    var family = document.getElementById('txt-family').value;
    var talk
    switch(family) {
        default: {
            talk = " "
            break
        }
        case value='B': {
            console.log('xin chào bố !')
            talk = `xin chào bố !`
            break
        }
        case value="M":{
            console.log('xin chào mẹ !')
            talk = `xin chào mẹ !`
            break
        }
        case value="A": {
            console.log('xin chào Anh trai !')
            talk = `xin chào Anh trai !`
            break
        }
        case value="E": {
            console.log('xin chào Em gái !')
            talk = `xin chào Em gái !`
            break
        } 
    }
    document.querySelector('.say__hi').innerHTML = `<div>${talk}</div>`
})


// Bài 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.

/**
 Mô hìn h3 khối
 Input: tạo ra 3 nút để người dùng nhập các số nguyên

 Xử lí: 
 tạo biết tính toán để kiểm tra dữ liệu người dùng nhập là số chẵn hay số lẻ
 tạo 2 biến để lưu giữ số đếm của các số chẵn và số lẻ
 kiểm tra các số chẵn bằng các trường hợp cụ thể và count+=1 và dùng tổng số nhập trừ đi số chẵn để lấy được số đếm của số lẻ

 output: hiển thị ra kết quả số chẵn và số lẻ
*/
 document.querySelector('.claculatorEvenOdd').addEventListener('click',function clacevenOdd(){
    console.log('yes')

    //get number from input
    var NumN1 = document.getElementById('txt-songuyen1-b3').value * 1
    var NumN2 = document.getElementById('txt-songuyen2-b3').value * 1
    var NumN3 = document.getElementById('txt-songuyen3-b3').value * 1

    var InputQuantity = [NumN1,NumN1,NumN3].length

    var clacNumN1 = NumN1 % 2;
    var clacNumN2 = NumN2 % 2;
    var clacNumN3 = NumN3 % 2;

    var evenNumber = 0;
    var oddNumber = 0;

    if (clacNumN1 !== 0 && clacNumN2 !== 0 && clacNumN3 !== 0){
        console.log('số chẵn',evenNumber, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    }
    else if (clacNumN1 === 0 && clacNumN2 !== 0 && clacNumN3 !== 0) {
        console.log('số chẵn',evenNumber+=1, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    } 
    else if (clacNumN1 !== 0 && clacNumN2 === 0 && clacNumN3 !== 0) {
        console.log('số chẵn',evenNumber+=1, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    } 
    else if (clacNumN1 !== 0 && clacNumN2 !== 0 && clacNumN3 === 0) {
        console.log('số chẵn',evenNumber+=1, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    } 
    else if(clacNumN1 === 0 && clacNumN2 === 0 && clacNumN3 !== 0) {
        console.log('số chẵn',evenNumber+=2, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    } 
    else if (clacNumN1 === 0 && clacNumN2 !== 0 && clacNumN3 === 0) {
        console.log('số chẵn',evenNumber+=2, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    } 
    else if (clacNumN1 !== 0 && clacNumN2 === 0 && clacNumN3 === 0) {
        console.log('số chẵn',evenNumber+=2, "số lẽ",oddNumber = (InputQuantity - evenNumber)) 
    } else {
        console.log('số chẵn', evenNumber+=3, "số lẽ",oddNumber = (InputQuantity - evenNumber))
    }
    document.querySelector('.demSo').innerHTML = `<div>có ${evenNumber} số chẵn và ${oddNumber} số lẽ </div>`
})


// Bài 4: Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?
/**
 Mô hìn h3 khối
 Input: tạo ra 3 nút để người dùng nhập nhập 3 cạnh dài

 Xử lí: 
 xét các trường hợp cho các cạnh dài của tam giác để phân loại đó là tam giác gì
 sử dụng định lý pytago để nhận biết có phải tam giác vuông không.
 so sánh 2 cạnh dài bằng nhau để nhận biết tam đó có phải là tam giác cân không.
 so sánh 3 cạnh có bằng nhau không để nhận biết tam giác đó có phải là tam giác đều k

 output: trả về tam giác đó là tam giác gì và không nằm trong trường hợp nào thì k biết
*/
document.querySelector('.duDoan').addEventListener('click',function TamGiac(){
    
    var edge1 = document.getElementById('txt-canh-1').value * 1
    var edge2 = document.getElementById('txt-canh-2').value * 1
    var edge3 = document.getElementById('txt-canh-3').value * 1

    var triangle
    if (Math.pow(edge3,2) === Math.pow(edge1,2) + Math.pow(edge2,2)) {
        triangle = `tam giác vuông`
    } else if (edge1 !== edge2 && edge1 !== edge3 && edge2 === edge3) {
        triangle = `tam giác cân`
    } else if (edge2 !== edge1 && edge2 !== edge3 && edge3 === edge1) {
        triangle = `tam giác cân`
    } else if (edge3 !== edge1 && edge3 !== edge2 && edge1 === edge2) {
        triangle = `tam giác cân`
    } else if (edge1 === edge3 && edge1 === edge2 && edge3 === edge2) {
        triangle = `tam giác đều`
    } else {
        triangle = `tam giác nào đó`
    } 

    document.querySelector('.DuDoanTamGiac').innerHTML = `<div>Đó là ${triangle}</div>`
})

